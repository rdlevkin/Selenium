package pages;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BusinessTripPage extends BasePage {

    @FindBy(xpath = "//a[@title='Создать командировку']")
    private WebElement newTripButton;

    @FindBy(xpath = "//h1[@class='user-name']")
    private WebElement createTripTitle;

    @Step("Нажатие на кнопку \"Создать командировку\"")
    public BusinessTripPage createBusinessTrip() {
        newTripButton.click();
        loading();
        Assertions.assertEquals("Создать командировку", createTripTitle.getText());
        return this;
    }
}
