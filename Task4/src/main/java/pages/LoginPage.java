package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//form[@id='login-form']")
    private WebElement formElement;

    @FindBy(xpath = "//input[@name='_username']")
    private WebElement loginInputElement;

    @FindBy(xpath = "//input[@name='_password']")
    private WebElement passwordInputElement;

    @FindBy(xpath = "//button[@name='_submit']")
    private WebElement submitButton;

    @FindBy(xpath = "//h1[@class='oro-subtitle']")
    private WebElement subtitle;

    @Step("Открытие страницы авторизации. Ввод логина и пароля")
    public LoginPage enterLoginAndPassword(String login, String password) {
        waitUntilElementToBeVisible(formElement);
        loginInputElement.sendKeys(login);
        passwordInputElement.sendKeys(password);
        return this;
    }

    @Step("Нажатие на кнопку \"Войти\"")
    public LoginPage submitClick() {
        submitButton.click();
        waitUntilElementToBeVisible(subtitle);
        return this;
    }
}
