package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MainPage extends BasePage {

    @FindBy(xpath = "//ul[contains(@class,'main-menu')]/li/a/span[text()='Расходы']")
    private WebElement costsNavigationElement;

    @FindBy(xpath = "//ul[contains(@class,'main-menu')]/li/a/span[text()='Расходы']" +
            "/ancestor::li//ul[contains(@class,'menu_level_1')]")
    private WebElement dropDownElement;

    @FindBy(xpath = "//span[text()='Командировки']")
    private WebElement businessTripElement;

    @Step("Нажатие на \"Расходы\" в навигационном меню")
    public MainPage costsClick() {
        costsNavigationElement.click();
        return this;
    }

    @Step("Нажатие на \"Командировки\"")
    public MainPage businessTripClick() {
        waitUntilElementToBeVisible(dropDownElement);
        businessTripElement.click();
        loading();
        return this;
    }
}
