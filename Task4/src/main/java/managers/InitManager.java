package managers;

import java.time.Duration;

import static utils.PropertyConst.*;

public class InitManager {

    private static final PropertyManager properties = PropertyManager.getInstance();

    private static final DriverManager driverManager = DriverManager.getInstance();

    public static void init() {
        driverManager.getDriver().manage().window().maximize();
        driverManager.getDriver().manage().timeouts()
                .implicitlyWait(Duration.ofSeconds(Integer.parseInt(properties.getProperty(IMPLICITLY_WAIT))));
        driverManager.getDriver().manage().timeouts()
                .pageLoadTimeout(Duration.ofSeconds(Integer.parseInt(properties.getProperty(PAGE_LOAD_TIMEOUT))));
        driverManager.getDriver().get(PropertyManager.getInstance().getProperty(BASE_URL));
    }

    public static void quitFramework() {
        driverManager.quitDriver();
    }
}
