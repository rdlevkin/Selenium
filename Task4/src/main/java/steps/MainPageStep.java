package steps;

import io.cucumber.java.en.And;
import pages.MainPage;

public class MainPageStep {

    private final MainPage mainPage = new MainPage();

    @And("^Нажатие на \"Расходы\" в навигационном меню$")
    public void costsClickCucumber() {
        mainPage.costsClick();
    }

    @And("^Нажатие на \"Командировки\"$")
    public void businessTripClickCucumber() {
        mainPage.businessTripClick();
    }
}
