package steps;

import io.cucumber.java.en.And;
import pages.CreateBusinessTripPage;

public class CreateBusinessTripStep {

    private final CreateBusinessTripPage createBusinessTripPage = new CreateBusinessTripPage();

    @And("^Выбор подразделения$")
    public void selectFirstSubdivisionCucumber() {
        createBusinessTripPage.selectFirstSubdivision();
    }

    @And("^Отрытие списка с организациями$")
    public void openOrganizationListCucumber() {
        createBusinessTripPage.openOrganizationList();
    }

    @And("^Выбор организации - \"([^\"]*)\"$")
    public void selectOrganizationCucumber(String organization) {
        createBusinessTripPage.selectOrganization(organization);
    }

    @And("^Поставить чек-бокс \"Заказ билетов\"$")
    public void orderingTicketsClickCucumber() {
        createBusinessTripPage.orderingTicketsClick();
    }

    @And("^Заполение полей \"Города выбытия\" - \"([^\"]*)\" и \"Город прибытия\" - \"([^\"]*)\"$")
    public void fillCitiesCucumber(String departure, String arrival) {
        createBusinessTripPage.fillCities(departure, arrival);
    }

    @And("^Заполение полей \"Дата выезда\" - \"([^\"]*)\" и \"Дата возвращения\" - \"([^\"]*)\"$")
    public void fillDatesCucumber(String begin, String end) {
        createBusinessTripPage.fillDates(begin, end);
    }

    @And("^Нажатие на конопку \"Сохранить и закрыть\"$")
    public void saveAndCloseClickCucumber() {
        createBusinessTripPage.saveAndCloseClick();
    }

    @And("^Проверка ошибок в разделе \"Командированные сотрудники\"$")
    public void checkOnEmployeeTypesErrorCucumber() {
        createBusinessTripPage.checkOnEmployeeTypesError();
    }
}
