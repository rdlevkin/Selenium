package steps;

import io.cucumber.java.en.And;
import pages.LoginPage;

public class LoginStep {

    private final LoginPage loginPage = new LoginPage();

    @And("^Страница авторизации: вводим логин \"([^\"]*)\" и пароль \"([^\"]*)\"$")
    public void enterLoginAndPasswordCucumber(String login, String password) {
        loginPage.enterLoginAndPassword(login, password);
    }

    @And("^Нажатие на кнопку \"Войти\"$")
    public void submitClickCucumber() {
        loginPage.submitClick();
    }
}
