package steps;

import io.cucumber.java.en.And;
import pages.BusinessTripPage;

public class BusinessTripStep {

    private final BusinessTripPage businessTripPage = new BusinessTripPage();

    @And("Нажатие на кнопку \"Создать командировку\"")
    public void createBusinessTripCucumber() {
        businessTripPage.createBusinessTrip();
    }
}
