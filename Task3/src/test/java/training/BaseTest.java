package training;

import managers.DriverManager;
import managers.InitManager;
import managers.PageManager;
import managers.PropertyManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import static utils.PropertyConst.BASE_URL;

public class BaseTest {

    protected final PageManager pageManager = PageManager.getInstance();

    private final DriverManager driverManager = DriverManager.getInstance();

    @BeforeAll
    public static void init() {
        InitManager.init();
    }

    @BeforeEach
    public void beforeEach() {
        driverManager.getDriver().get(PropertyManager.getInstance().getProperty(BASE_URL));
    }

    @AfterAll
    public static void cleanUp() {
        InitManager.quitFramework();
    }

    protected String getProperty(String property) {
        return PropertyManager.getInstance().getProperty(property);
    }
}
