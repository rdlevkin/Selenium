package training;

import extension.AllureExtension;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.TmsLink;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static utils.PropertyConst.*;

@DisplayName("Оформление заявки на командировку")
@ExtendWith({AllureExtension.class})
public class TravelRequestsTest extends BaseTest {

    @Test
    @Severity(SeverityLevel.MINOR)
    @TmsLink("Appline test")
    @Link("http://training.appline.ru/user/login")
    public void secondedEmployeesMessage() {
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DATE, 1);
        Calendar weekLater = Calendar.getInstance();
        weekLater.add(Calendar.DATE, 7);

        pageManager.getLoginPage()
                .enterLoginAndPassword(getProperty(LOGIN), getProperty(PASSWORD))
                .submitClick();
        pageManager.getMainPage()
                .costsClick()
                .businessTripClick();
        pageManager.getBusinessTripPage()
                .createBusinessTrip();
        pageManager.getCreateBusinessTripPage()
                .selectFirstSubdivision()
                .openOrganizationList().selectOrganization("(Хром) Призрачная Организация Охотников")
                .orderingTicketsClick()
                .fillCities("Россия, Москва", "Россия, Санкт-Петербург")
                .fillDates(new SimpleDateFormat("dd.MM.yyyy").format(tomorrow.getTime()),
                        new SimpleDateFormat("dd.MM.yyyy").format(weekLater.getTime()))
                .saveAndCloseClick()
                .checkOnEmployeeTypesError();
    }
}