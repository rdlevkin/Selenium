package pages;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class CreateBusinessTripPage extends BasePage {

    @FindBy(xpath = "//div[contains(@id, 'crm_business_trip_businessUnit-uid')]")
    private WebElement subdivisionElement;

    @FindBy(xpath = "//a[@id='company-selector-show']")
    private WebElement openOrganizationListElement;

    @FindBy(xpath = "//div[@id='company-selector']")
    private WebElement dropDownListElement;

    @FindBy(xpath = "//label[text()='Заказ билетов']/parent::div/input")
    private WebElement orderingTicketsCheckBox;

    @FindBy(xpath = "//input[contains(@id, 'crm_business_trip_departureCity')]")
    private WebElement departureCityField;

    @FindBy(xpath = "//input[contains(@id, 'crm_business_trip_arrivalCity-uid')]")
    private WebElement arrivalCityField;

    @FindBy(xpath = "//input[contains(@id, 'date_selector_crm_business_trip_departureDatePlan')]")
    private WebElement departureDateElement;

    @FindBy(xpath = "//input[contains(@id, 'date_selector_crm_business_trip_returnDatePlan')]")
    private WebElement returnDateElement;

    @FindBy(xpath = "//button[contains(text(), 'Сохранить и закрыть')]")
    private WebElement saveAndCloseButton;

    @FindBy(xpath = "//span[contains(@class, 'validation-failed') and contains(text(), 'не может быть пустым')]")
    private WebElement emptyFieldError;

    @Step("Выбор подразделения")
    public CreateBusinessTripPage selectFirstSubdivision() {
        subdivisionElement.click();
        String elementId = subdivisionElement.findElements(
                By.xpath("//div[contains(@id, 'crm_business_trip_businessUnit-uid')]//option[not(@selected='selected')]"))
                .get(0)
                .getAttribute("value");
        WebElement firstSubdivisionElement = subdivisionElement.findElement(
                By.xpath(".//option[@value=" + elementId + "]"));
        waitUntilElementToBeClickable(firstSubdivisionElement);
        firstSubdivisionElement.click();
        String subdivisionData = subdivisionElement.findElement(By.xpath("./span")).getText();
        Assertions.assertEquals(firstSubdivisionElement.getText(), subdivisionData);
        return this;
    }

    @Step("Отрытие списка с организациями")
    public CreateBusinessTripPage openOrganizationList() {
        openOrganizationListElement.click();
        waitUntilElementToBeVisible(dropDownListElement);
        return this;
    }

    @Step("Выбор организации из списка")
    public CreateBusinessTripPage selectOrganization(String organizationName) {
        dropDownListElement.click();
        driverManager.getDriver().findElement(
                By.xpath("//div[contains(text(), '" + organizationName + "')]/parent::li"))
                .click();
        String selectedOrganization = dropDownListElement.findElement(By.xpath(".//span[@class='select2-chosen']"))
                .getText();
        Assertions.assertEquals(organizationName, selectedOrganization);
        return this;
    }

    @Step("Поставить чек-бокс \"Заказ билетов\"")
    public CreateBusinessTripPage orderingTicketsClick() {
        orderingTicketsCheckBox.click();
        Assertions.assertTrue(orderingTicketsCheckBox.isSelected());
        return this;
    }

    @Step("Заполение полей \"Города выбытия\" и \"Город прибытия\"")
    public CreateBusinessTripPage fillCities(String departure, String arrival) {
        fillInputField(departureCityField, departure);
        fillInputField(arrivalCityField, arrival);
        return this;
    }

    @Step("Заполение полей \"Дата выезда\" и \"Дата возвращения\"")
    public CreateBusinessTripPage fillDates(String begin, String end) {
        fillDateField(departureDateElement, begin);
        fillDateField(returnDateElement, end);
        return this;
    }

    @Step("Нажатие на конопку \"Сохранить и закрыть\"")
    public CreateBusinessTripPage saveAndCloseClick() {
        saveAndCloseButton.click();
        loading();
        return this;
    }

    @Step("Проверка ошибок в разделе \"Командированные сотрудники\"")
    public CreateBusinessTripPage checkOnEmployeeTypesError() {
        waitUntilElementToBeVisible(emptyFieldError);
        List<WebElement> employeeTypesElements = driverManager.getDriver().findElements(
                By.xpath("//h4[contains(text(), 'Командированные сотрудники')]/parent::div//div[contains(@class, 'responsive-cell')]"));
        employeeTypesElements.forEach(element -> {
            scrollToElementJs(element);
            String title = element.findElement(By.xpath(".//h5/span")).getText();
            WebElement errorElement = element.findElement(By.xpath(".//span[contains(@class, 'validation-failed')]"));
            Assertions.assertEquals("Список командируемых сотрудников не может быть пустым",
                    errorElement.getText(),
                    "Раздел \"" + title + "\"");
        });
        return this;
    }
}
