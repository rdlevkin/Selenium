package managers;

import pages.BusinessTripPage;
import pages.CreateBusinessTripPage;
import pages.LoginPage;
import pages.MainPage;

public class PageManager {

    private static PageManager pageManager;

    private LoginPage loginPage;

    private MainPage mainPage;

    private BusinessTripPage businessTripPage;

    private CreateBusinessTripPage createBusinessTripPage;

    private PageManager() { }

    public static PageManager getInstance() {
        if (pageManager == null) {
            pageManager = new PageManager();
        }
        return pageManager;
    }

    public LoginPage getLoginPage() {
        if (loginPage == null) {
            loginPage = new LoginPage();
        }
        return loginPage;
    }

    public MainPage getMainPage() {
        if (mainPage == null) {
            mainPage = new MainPage();
        }
        return mainPage;
    }

    public BusinessTripPage getBusinessTripPage() {
        if (businessTripPage == null) {
            businessTripPage = new BusinessTripPage();
        }
        return businessTripPage;
    }

    public CreateBusinessTripPage getCreateBusinessTripPage() {
        if (createBusinessTripPage == null) {
            createBusinessTripPage = new CreateBusinessTripPage();
        }
        return createBusinessTripPage;
    }
}
