package utils;

public class PropertyConst {

    public static final String BASE_URL = "base.url";

    public static final String LOGIN = "login";

    public static final String PASSWORD = "password";

    public static final String TYPE_BROWSER = "type.browser";

    public static final String PATH_CHROME_DRIVER_WINDOWS = "path.chrome.driver.windows";

    public static final String PATH_CHROME_DRIVER_MAC = "path.chrome.driver.mac";

    public static final String PATH_CHROME_DRIVER_UNIX = "path.chrome.driver.unix";

    public static final String IMPLICITLY_WAIT = "implicitly.wait";

    public static final String PAGE_LOAD_TIMEOUT = "page.load.timeout";
}
