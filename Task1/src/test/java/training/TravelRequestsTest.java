package training;

import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.List;

public class TravelRequestsTest {

    private static WebDriver driver;
    private static WebDriverWait wait;

    @BeforeAll
    public static void init() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
    }

    @BeforeEach
    public void beforeEach() {
        driver.get("http://training.appline.ru/user/login");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
    }

    @Test
    public void test() {
        // Авторизация на странице
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//form[@id='login-form']"))));
        driver.findElement(By.xpath("//input[@name='_username']")).sendKeys("Sekretar Kompanii");
        driver.findElement(By.xpath("//input[@name='_password']")).sendKeys("testing");
        driver.findElement(By.xpath("//button[@name='_submit']")).click();
        String mainPageTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(
                By.xpath("//h1[@class='oro-subtitle']"))))
                .getText();
        Assertions.assertEquals("Панель быстрого запуска", mainPageTitle);

        //Расходы -> Командировки
        WebElement dropDownElement = driver.findElement(By.xpath("//ul[contains(@class,'main-menu')]/li/a/span[text()='Расходы']"));
        dropDownElement.click();
        waitUntilElementToBeVisible(dropDownElement.findElement(By.xpath("./ancestor::li//ul[contains(@class,'menu_level_1')]")));
        driver.findElement(By.xpath("//ul[contains(@class,'menu_level_1')]/li/a/span[text()='Командировки']")).click();
        loading();

        // Создать командировку
        driver.findElement(By.xpath("//a[@title='Создать командировку']")).click();
        loading();
        String tripTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(
                By.xpath("//h1[@class='user-name']")))).getText();
        Assertions.assertEquals("Создать командировку", tripTitle);

        // Заполнение полей
        // Подразделение
        WebElement subdivisionElement = driver.findElement(By.xpath("//div[contains(@id, 'crm_business_trip_businessUnit-uid')]"));
        subdivisionElement.click();
        List<WebElement> subdivisionList = subdivisionElement.findElements(
                By.xpath("//div[contains(@id, 'crm_business_trip_businessUnit-uid')]//option[not(@selected='selected')]"));
        WebElement firstSubdivisionElement = subdivisionElement.findElement(By.xpath(".//option[@value=" +
                subdivisionList.get(0).getAttribute("value") + "]"));
        waitUntilElementToBeClickable(firstSubdivisionElement);
        firstSubdivisionElement.click();
        String subdivisionData = subdivisionElement.findElement(By.xpath("./span")).getText();
        Assertions.assertEquals(subdivisionList.get(0).getText(), subdivisionData);

        // Организация
        driver.findElement(By.xpath("//a[@id='company-selector-show']")).click();
        waitUntilElementToBeVisible(driver.findElement(By.xpath("//div[@id='company-selector']")));
        WebElement organizationElement = driver.findElement(By.xpath("//div[contains(@id, 's2id_crm_business_trip_companySelector-uid')]"));
        organizationElement.click();
        WebElement organizationList = findElementUntilToBeVisible("//div[contains(@class, 'select2-drop-active')]");
        organizationList.findElement(By.xpath("//div[contains(text(), '(Хром) Призрачная Организация Охотников')]/parent::li")).click();
        String selectedOrganization = organizationElement.findElement(By.xpath(".//span[@class='select2-chosen']")).getText();
        Assertions.assertEquals("(Хром) Призрачная Организация Охотников", selectedOrganization);

        // Заказ билетов
        WebElement ticketBookingElement = driver.findElement(By.xpath("//label[text()='Заказ билетов']/parent::div/input"));
        ticketBookingElement.click();
        Assertions.assertTrue(ticketBookingElement.isSelected());

        // Города
        fillInput(driver.findElement(By.xpath("//input[contains(@id, 'crm_business_trip_departureCity')]")), "Россия, Москва");
        fillInput(driver.findElement(By.xpath("//input[contains(@id, 'crm_business_trip_arrivalCity-uid')]")), "Россия, Санкт-Петербург");

        // Даты
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        fillDate(driver.findElement(By.xpath("//input[contains(@id, 'date_selector_crm_business_trip_departureDatePlan')]")),
                new SimpleDateFormat("dd.MM.yyyy").format(c.getTime()));
        c.add(Calendar.DATE, 6);
        fillDate(driver.findElement(By.xpath("//input[contains(@id, 'date_selector_crm_business_trip_returnDatePlan')]")),
                new SimpleDateFormat("dd.MM.yyyy").format(c.getTime()));

        // Сохранить и закрыть
        WebElement saveButton = driver.findElement(By.xpath("//button[contains(text(), 'Сохранить и закрыть')]"));
        saveButton.click();
        loading();

        waitUntilElementToBeVisible(driver.findElement(By.xpath("//span[contains(@class, 'validation-failed') and contains(text(), 'не может быть пустым')]")));
        List<WebElement> employeeTypesElements = driver.findElements(
                By.xpath("//h4[contains(text(), 'Командированные сотрудники')]/parent::div//div[contains(@class, 'responsive-cell')]"));
        employeeTypesElements.forEach(element -> {
            scrollToElementJS(element);
            String title = element.findElement(By.xpath(".//h5/span")).getText();
            WebElement errorElement = element.findElement(By.xpath(".//span[contains(@class, 'validation-failed')]"));
            Assertions.assertEquals("Список командируемых сотрудников не может быть пустым",
                    errorElement.getText(),
                    "Раздел \"" + title + "\"");
        });
    }

    @AfterEach
    public void after() {
        driver.quit();
    }

    public void loading() {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='loader-mask shown']"))));
    }

    private void scrollToElementJS(WebElement element) {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    private void waitUntilElementToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    private void waitUntilElementToBeVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    private WebElement findElementUntilToBeVisible(String xpath) {
        waitUntilElementToBeVisible(driver.findElement(By.xpath(xpath)));
        return driver.findElement(By.xpath(xpath));
    }

    private void fillInput(WebElement element, String value) {
        scrollToElementJS(element);
        waitUntilElementToBeClickable(element);
        element.click();
        element.clear();
        element.sendKeys(value);
        boolean checkFlag = wait.until(ExpectedConditions.attributeContains(element, "value", value));
        Assertions.assertTrue(checkFlag);
    }

    private void fillDate(WebElement element, String value) {
        fillInput(element, value);
        element.sendKeys(Keys.ESCAPE);
    }
}