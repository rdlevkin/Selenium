package training;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/"},
        glue = {"steps"},
        tags = "@all",
        plugin = {"pretty", "summary"},
        snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class CucumberRunner { }
