@all
@severity=minor
@issue=1
@tmslink=Appline_test
Feature: Онлайн магазин regard.ru

  @1
  Scenario Outline: Поиск товара по имени
    Given Отрытие главной страницы
    And Раскрытие каталога
    And Отрытие раздела "<mainCategory>" -> "<nestedCategories>"
    And Указание минимальной стоимости товара в "<minPrice>" рублей
    And Выбор производителя - "<producer>"
    And Проверка, что на странице не более 24 товаров
    And Скопировать имя первого продукта в списке
    And Поиск продукта по названию
    And Проверка, что найден "1" товар
    And Проверка, найденный товар соответствует искомому

    Examples:
    | mainCategory         | nestedCategories | minPrice | producer |
    | Комплектующие для ПК | Видеокарты       | 20000    | Gigabyte |
    | Периферия            | Клавиатуры       | 2000     | A4Tech   |