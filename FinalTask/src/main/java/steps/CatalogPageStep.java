package steps;

import io.cucumber.java.en.And;
import pages.CatalogPage;
import pages.NavigationPage;

public class CatalogPageStep {

    private final CatalogPage catalogPage = new CatalogPage();

    private final NavigationPage navigationPage = new NavigationPage();

    private String firstProduct;

    @And("^Указание минимальной стоимости товара в \"([^\"]*)\" рублей$")
    public void setMinPriceCucumber(String minPrice) {
        catalogPage.setMinPrice(minPrice);
    }

    @And("^Выбор производителя - \"([^\"]*)\"$")
    public void selectProducerCucumber(String producer) {
        catalogPage.selectProducer(producer);
    }

    @And("^Проверка, что на странице не более 24 товаров$")
    public void checkForNumberOfFoundedProductsCucumber() {
        catalogPage.checkProductNumberOnPage(24);
    }

    @And("^Скопировать имя первого продукта в списке$")
    public void copyFirstProductCucumber() {
        catalogPage.copyFirstProduct();
        firstProduct = catalogPage.getSavedProduct();
    }

    @And("^Поиск продукта по названию$")
    public void findProductCucumber() {
        navigationPage.findProduct(firstProduct);
        catalogPage.waitForSearchRefreshPage();
    }

    @And("^Проверка, что найден \"([^\"]*)\" товар$")
    public void checkTotalProductsCucumber(int desiredNumber) {
        catalogPage.checkTotalProducts(desiredNumber);
    }

    @And("^Проверка, найденный товар соответствует искомому$")
    public void compareFoundedProductNamesCucumber() {
        catalogPage.compareFoundedProductNames(firstProduct);
    }
}
