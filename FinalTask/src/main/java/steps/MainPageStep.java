package steps;

import io.cucumber.java.en.And;
import pages.NavigationPage;
import pages.MainPage;

public class MainPageStep {

    private final MainPage mainPage = new MainPage();

    private final NavigationPage navigationPage = new NavigationPage();

    @And("^Отрытие главной страницы$")
    public void openMainPageCucumber() {
        mainPage.pageIsOpen();
    }

    @And("^Раскрытие каталога$")
    public void openCatalogCucumber() {
        navigationPage.openCatalog();
    }

    @And("^Отрытие раздела \"([^\"]*)\" -> \"([^\"]*)\"$")
    public void selectCategory(String mainCategory, String nestedCategories) {
        navigationPage.openCatalogSection(mainCategory, nestedCategories);
    }
}
