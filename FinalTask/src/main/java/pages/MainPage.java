package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BasePage {

    @FindBy(xpath =  "//a[contains(@class, 'Header_logo')]")
    private WebElement logo;

    public MainPage pageIsOpen() {
        waitUntilElementToBeVisible(logo);
        return this;
    }
}
