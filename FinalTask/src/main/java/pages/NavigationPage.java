package pages;

import io.cucumber.java.en_lol.WEN;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavigationPage extends BasePage {

    @FindBy(xpath = "//span[text()='Каталог']/parent::button")
    private WebElement catalogButton;

    @FindBy(xpath = "//div[contains(@class, 'CatalogPanel_inner')]")
    private WebElement catalogPanel;

    @FindBy(xpath = "//h1[contains(@class, 'PageTitle_title')]")
    private WebElement productPageHeader;

    @FindBy(xpath = "//input[@id='searchInput']")
    private WebElement searchBox;

    @FindBy(xpath = "//*[local-name() = 'svg' and contains(@class, 'Icons_search')]")
    private WebElement searchButton;

    @Step("Открытие каталога")
    public NavigationPage openCatalog() {
        catalogButton.click();
        waitUntilElementToBeVisible(catalogPanel);
        return this;
    }

    @Step("Открытие раздела")
    public NavigationPage openCatalogSection(String mainCategory, String nestedCategories) {
        WebElement mainCategoryList = catalogPanel.findElement(By.xpath(".//ul"));
        WebElement targetMainCategoryElement = mainCategoryList.findElement(
                By.xpath(".//div[contains(text(), '" + mainCategory + "')]"));
        action.moveToElement(targetMainCategoryElement).perform();

        String nestedCategoryHeader = catalogPanel.findElement(
                By.xpath(".//div[contains(@class, 'NestedCategories_wrap')]/h3//a"))
                .getText();
        Assertions.assertEquals(mainCategory, nestedCategoryHeader);

        WebElement targetNestedCategoryElement = catalogPanel.findElement(
                By.xpath("//a[contains(@class, 'Category_subTitle') and contains(text(), '" + nestedCategories +"')]"));
        targetNestedCategoryElement.click();
        waitUntilElementToBeVisible(productPageHeader);
        return this;
    }

    @Step("Поиск товара")
    public NavigationPage findProduct(String text) {
        fillInputField(searchBox, text, false);
        searchButton.click();
        return this;
    }
}
