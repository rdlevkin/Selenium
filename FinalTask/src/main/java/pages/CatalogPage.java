package pages;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class CatalogPage extends BasePage {

    private String prevTotal = "0";

    private String productBuffer;

    @Step("Ввод минимальной стоимости товара")
    public CatalogPage setMinPrice(String minPrice) {
        WebElement minPriceInput = driverManager.getDriver().findElement(
                By.xpath("//span[text()='Цена']/ancestor::section//input[@name='min']"));
        fillInputField(minPriceInput, minPrice, false);
        waitForCatalogUpdate();
        return this;
    }

    @Step("Выбор производителя товара")
    public CatalogPage selectProducer(String producer) {
        WebElement producerSection = waitUntilElementToBeVisible(driverManager.getDriver().findElement(
                By.xpath("//span[text()='Производитель']/ancestor::section")));
        WebElement clickableProducerCheckbox = producerSection.findElement(
                By.xpath(".//label[text()='" + producer + "']/ancestor::span/label[contains(@class, 'fakeCheckbox')]"));
        WebElement producerCheckbox = producerSection.findElement(
                By.xpath(".//label[text()='" + producer + "']/ancestor::span/input"));
        waitUntilElementToBeClickable(clickableProducerCheckbox).click();
        Assertions.assertTrue(producerCheckbox.isSelected());
        waitForCatalogUpdate();
        return this;
    }

    @Step("Проверка общего числа найденных товаров")
    public CatalogPage checkTotalProducts(int desiredNumber) {
        String totalProducts = driverManager.getDriver().findElement(
                By.xpath("//span[contains(@class, 'PageTitle_count')]"))
                .getText()
                .split(" ")[0];
        Assertions.assertEquals(desiredNumber, Integer.parseInt(totalProducts));
        return this;
    }

    @Step("Проверка числа товаров на странице")
    public CatalogPage checkProductNumberOnPage(int productsOnPage) {
        String totalProducts = driverManager.getDriver().findElement(
                By.xpath("//span[contains(@class, 'PageTitle_count')]"))
                .getText()
                .split(" ")[0];

        // Если найдено меньше 24 товаров, то кнопки для смены числа продуктов на странице нет
        if (Integer.parseInt(totalProducts) >= 24) {
            String currentProductsOnPage = driverManager.getDriver().findElement(
                    By.xpath("//span[contains(@class, 'Pagination_countSetter')]"))
                    .getText()
                    .split(" ")[1];
            Assertions.assertEquals(Integer.parseInt(currentProductsOnPage), productsOnPage);
        }
        return this;
    }

    @Step("Копирование первого элемента в списке товаров")
    public CatalogPage copyFirstProduct() {
        List<WebElement> products = driverManager.getDriver().findElements(By.xpath("//h6"));
        productBuffer = products.get(0).getText();
        return this;
    }

    @Step("Сравнение имен товаров")
    public CatalogPage compareFoundedProductNames(String anotherProduct) {
        String foundedProduct = driverManager.getDriver().findElement(By.xpath("//div[contains(@class, 'Card_wrap')]//h6"))
                .getText();
        Assertions.assertEquals(foundedProduct, anotherProduct);
        return this;
    }

    public String getSavedProduct() {
        return productBuffer;
    }

    public void waitForSearchRefreshPage() {
        prevTotal = "0";
        wait.until((ExpectedCondition<Boolean>) driver -> driverManager.getDriver()
                .findElements(By.xpath("//div[contains(@class, 'FilterTags_group__')]/span[text()='Поиск']"))
                .size() > 0);
        wait.until((ExpectedCondition<Boolean>) driver -> driverManager.getDriver()
                .findElements(By.xpath("//p[text()='В наличии']"))
                .size() > 0);
        waitForCatalogUpdate();
    }

    public void waitForCatalogUpdate() {
        WebElement resultTotalCount = driverManager.getDriver().findElement(By.xpath("//span[contains(@class, 'PageTitle_count__')]"));
        wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(resultTotalCount, prevTotal)));
        wait.until(ExpectedConditions.textToBePresentInElement(resultTotalCount, "товар"));
        prevTotal = resultTotalCount.getText().split(" ")[0];
    }
}
