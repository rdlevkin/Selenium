package managers;

public class PageManager {

    private static PageManager pageManager;

    private PageManager() { }

    public static PageManager getInstance() {
        if (pageManager == null) {
            pageManager = new PageManager();
        }
        return pageManager;
    }
}
