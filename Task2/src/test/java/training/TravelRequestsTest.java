package training;

import org.junit.jupiter.api.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static utils.PropertyConst.*;

public class TravelRequestsTest extends BaseTest {

    @Test
    public void secondedEmployeesMessage() {
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DATE, 1);
        Calendar weekLater = Calendar.getInstance();
        weekLater.add(Calendar.DATE, 7);

        pageManager.getLoginPage()
                .enterLoginAndPassword(getProperty(LOGIN), getProperty(PASSWORD))
                .submitClick();
        pageManager.getMainPage()
                .costsClick()
                .businessTripClick();
        pageManager.getBusinessTripPage()
                .createBusinessTrip();
        pageManager.getCreateBusinessTripPage()
                .selectFirstSubdivision()
                .openOrganizationList().selectOrganization("(Хром) Призрачная Организация Охотников")
                .orderingTicketsClick()
                .fillCities("Россия, Москва", "Россия, Санкт-Петербург")
                .fillDates(new SimpleDateFormat("dd.MM.yyyy").format(tomorrow.getTime()),
                           new SimpleDateFormat("dd.MM.yyyy").format(weekLater.getTime()))
                .saveAndCloseClick()
                .checkOnEmployeeTypesError();
    }
}