package pages;

import managers.DriverManager;
import managers.PageManager;
import managers.PropertyManager;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {

    protected final DriverManager driverManager = DriverManager.getInstance();

    protected final PageManager pageManager = PageManager.getInstance();

    protected final Actions action = new Actions(driverManager.getDriver());

    protected final JavascriptExecutor js = (JavascriptExecutor) driverManager.getDriver();

    protected final WebDriverWait wait = new WebDriverWait(driverManager.getDriver(),
            Duration.ofSeconds(10), Duration.ofSeconds(2));

    protected final PropertyManager properties = PropertyManager.getInstance();

    public BasePage() {
        PageFactory.initElements(driverManager.getDriver(), this);
    }

    protected WebElement scrollToElementJs(WebElement element) {
        js.executeScript("arguments[0].scrollIntoView(true);", element);
        return element;
    }

    public WebElement scrollWithOffset(WebElement element, int x, int y) {
        String code = "window.scroll(" + (element.getLocation().x + x) + ","
                + (element.getLocation().y + y) + ");";
        ((JavascriptExecutor) driverManager.getDriver()).executeScript(code, element, x, y);
        return element;
    }

    protected WebElement waitUntilElementToBeClickable(WebElement element) {
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitUntilElementToBeVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected void fillInputField(WebElement field, String value) {
        scrollToElementJs(field);
        waitUntilElementToBeClickable(field).click();
        field.clear();
        field.sendKeys(value);
        boolean checkFlag = wait.until(ExpectedConditions.attributeContains(field, "value", value));
        Assertions.assertTrue(checkFlag);
    }

    protected void fillDateField(WebElement field, String value) {
        scrollToElementJs(field);
        field.sendKeys(value);
        field.sendKeys(Keys.ESCAPE);
    }

    protected void loading() {
        waitUntilElementToBeVisible(driverManager.getDriver()
            .findElement(By.xpath("//div[@class='loader-mask shown']")));
    }
}
